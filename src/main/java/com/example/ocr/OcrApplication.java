package com.example.ocr;

import net.sourceforge.tess4j.Tesseract;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;

@SpringBootApplication
public class OcrApplication {

    public static void main(String[] args) {
        SpringApplication.run(OcrApplication.class, args);
        try {
            File image = new File("src/main/resources/images/cach-trinh-bay-van-ban-hanh-chinh-800x500_c.png");
            Tesseract tesseract = new Tesseract();
            tesseract.setDatapath("src/main/resources/tessdata");
            tesseract.setLanguage("vie");
            tesseract.setPageSegMode(1);
            tesseract.setOcrEngineMode(3);
            String result = tesseract.doOCR(image);
            System.out.println(result);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
