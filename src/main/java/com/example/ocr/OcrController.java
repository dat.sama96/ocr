package com.example.ocr;

import net.sourceforge.tess4j.Tesseract;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.File;

@RequestMapping("/ocr/api")
public class OcrController {
    @PostMapping
    public String getTextFromImage(@RequestParam File fileImage) throws Exception {
        try {
            Tesseract tesseract = new Tesseract();
            tesseract.setDatapath("src/main/resources/tessdata");
            tesseract.setLanguage("vie");
            /*
                PSM_OSD_ONLY	0	Orientation and script detection only.
                PSM_AUTO_OSD	1	Automatic page segmentation with orientation and script detection. (OSD)
                PSM_AUTO_ONLY	2	Automatic page segmentation, but no OSD, or OCR.
                PSM_AUTO	3	Fully automatic page segmentation, but no OSD.
                PSM_SINGLE_COLUMN	4	Assume a single column of text of variable sizes.
                PSM_SINGLE_BLOCK_VERT_TEXT	5	Assume a single uniform block of vertically aligned text.
                PSM_SINGLE_BLOCK	6	Assume a single uniform block of text. (Default.)
                PSM_SINGLE_LINE	7	Treat the image as a single text line.
                PSM_SINGLE_WORD	8	Treat the image as a single word.
                PSM_CIRCLE_WORD	9	Treat the image as a single word in a circle.
                PSM_SINGLE_CHAR	10	Treat the image as a single character.
                PSM_SPARSE_TEXT	11	Find as much text as possible in no particular order.
                PSM_SPARSE_TEXT_OSD	12	Sparse text with orientation and script det.
                PSM_RAW_LINE	13	Treat the image as a single text line, bypassing hacks that are Tesseract-specific.
            */
            tesseract.setPageSegMode(1);
            /*
                0 legacy engine only
                1 neural nets LSTM engine only
                2 legacy + LSTM engines
                3 default, based on what is available
            */
            tesseract.setOcrEngineMode(1);
            return tesseract.doOCR(fileImage);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
}
